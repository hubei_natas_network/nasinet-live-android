package com.nasinet.live.ui.os;

import com.nasinet.live.model.entity.HotLive;

public interface OnItemClickListener {
    void onItemClick(HotLive live);

}
