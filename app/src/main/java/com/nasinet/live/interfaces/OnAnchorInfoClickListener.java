package com.nasinet.live.interfaces;

import com.nasinet.live.model.entity.RankAnchorItem;

public interface OnAnchorInfoClickListener {

    void onAnchorAvatarClick(RankAnchorItem live);


}
