package com.nasinet.live.contract;

/**
 * Created by cxf on 2018/9/25.
 */

public interface MainAppBarLayoutListener {
    void onOffsetChanged(float rate);
}
